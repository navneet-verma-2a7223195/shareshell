<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=G-Y7VQ4L5MXN"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-Y7VQ4L5MXN');
</script> -->

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="<?=base_url();?>assets/css/normalize.css?v=3.3">
        <link rel="stylesheet" href="<?=base_url();?>assets/css/font-awesome.min.css">
        <!--  -->
        <link rel="stylesheet" href="<?=base_url();?>assets/css/fontello.css">
        <link href="<?=base_url();?>assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
        <link href="<?=base_url();?>assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
        <link href="<?=base_url();?>assets/css/animate.css" rel="stylesheet" media="screen">
        <!--  -->
        <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap-select.min.css"> 
        
        <link rel="stylesheet" href="<?=base_url();?>assets/bootstrap/css/bootstrap.min.css">
        <!--  -->
        <link rel="stylesheet" href="<?=base_url();?>assets/css/icheck.min_all.css">
        <!--  -->
        <link rel="stylesheet" href="<?=base_url();?>assets/css/price-range.css">
        <link rel="stylesheet" href="<?=base_url();?>assets/css/owl.carousel.css">  
        <link rel="stylesheet" href="<?=base_url();?>assets/css/owl.theme.css">
        <link rel="stylesheet" href="<?=base_url();?>assets/css/owl.transitions.css?v=3.3">
        <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css?v=3.4" id="link_import_style">
        <link rel="stylesheet" href="<?=base_url();?>assets/css/responsive.css?v=3.3">